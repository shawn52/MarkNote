<?php
	//MarkNote 轻量级云记事本系统
	if( !file_exists('config.php') ){
		header("Location: include/install.php");
		exit();
	}

	
	require_once('common.php');
	require 'include/user.php';

	if( isset($_GET['type']) ){
		$type = $_GET['type'];
	}else{
		$type = 'home';
	}

	// echo 'index.php<br/>type='.$type.'<br/>';

	switch ($type) {
		case 'home':
			if(hasLogin()){
				require 'edit.php';
			}
			else{
				require 'login.php';
			}
			break;
		case 'show':
			require 'show.php';
			break;
		default:
			# code...
			break;
	}


