MarkNote - Cloud Markdown Notebook
=========================
v2 - beta 03
--------------
+添加了共享功能

修改如下
* 修复了Mathjax的配置错误
* 优化了Mathjax代码
* 优化了Prism代码
* css统一调整到file目录

v2 - beta 01
--------------
+添加了参数设置功能，然后对应的一些参数都配置化了。

*上次添加的数据库编码代码放的位置有问题完善了下。

代码总体上还是乱乱的，不想费功夫整理了，完善能用最重要。目前的版本基本可用，安全性欠妥，但自己使用没问题。
屏蔽了注册功能，需要注册看wiki。__安全性有很大的问题，没去修复，强烈建议自己使用。__

__交流QQ群：1294822__

v2 - Alpha 02
--------------
需要一个简单的记事本来记录日常，不想blog那样正式，只是自己随笔。
好记性不如烂笔头，也需要随时记录技术心得、代码脚本什么的。最好可以分类或者支持tags。
MarkNote正是这样一个简单的软件，v2版本确实写的糙了点，bug比较多，原作者说高考（唉，俺们上高中没这水平也没有时间捣鼓这些啊）后规划v3版。可等不及了，就在这个版本之上缝缝补补吧。

也不知道做了哪些修改了，目前也只是半成品，下面是记得做的修改

+ js可以升级的基本是目前最新版了
+ 模板化，分离了html和php脚本（用到的分离了，没用到的再说）
+ 中文化了。不过还是原来的英文比较好看看，汉化就汉化了吧。
+ 移动列表时对服务器的两次请求改一次
+ 搜索功能修复
+ 部分弹出框改成了div的，好看了很多。
+ 数据库保存的中文乱码，一句代码修复了下。
+ 支持公式的部分，修改好后发现一般用不到还影响加载速度就注释掉了。

暂时只记得改了上面那些了。__如果你在用这个项目，欢迎前来提交改进意见,会酌情修改。__

v2 - Alpha 01
--------------
Marknote is a web-based cloud MarkNote notebook system

![Screenshot](http://i.imgur.com/f4tt1aO.png)

Features
--------------
- Write your MarkDown note in a ACE editor with a live preview
- Save your note in a notebook or directly in you notelist
- Free to drag your note between notebook and other notebook
- The preview support Code highlight, Table, MathJax and other extensions
- A complete user system
- Autosave your note
- A friendly AJAX UI
- More features will be coming soon ...
